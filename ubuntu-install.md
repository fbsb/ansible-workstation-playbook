# Ubuntu install

```
partitions:
  efi: 1GiB
  boot: 1GiB
  cryptdata: 100%FREE

lvm:
  cryptdata: # pv
    ssd: # vg
      swap: 1.5xPhysMem
      system: 25GiB # for rootfs with optional snapshots
      data: 50%FREE # for home and other data

btrfs:
  system:
    @
    @/.snapshots
    @/.snapshots/1/
  data:
    @home
    @var
    @usr_local
    @srv
    @root
    @opt
```

## booting

Boot an ubuntu usb disk

Follow ubuntu installer until setting keyboard layout, then quit installer.

Open a terminal (ctrl + alt + t) then elevate privileges to root (`sudo -i`)

## disk partitions (optional)

Skip this step if your disk has already been partitioned

Find the disk you want to install ubuntu to, usually the largest disk in the system

```
lsblk -t -l -b -o NAME,SIZE,TYPE --sort SIZE -p | grep disk
```

Set `DISK` to the disk the system will be installed to

```
export DISK=/dev/vda

parted $DISK mklabel gpt
# Confirm with Yes
parted $DISK mkpart primary fat32 1Mib 1025MiB
parted $DISK mkpart primary ext3 1025MiB 2049MiB
parted $DISK mkpart primary 2049MiB 100%
parted $DISK name 1 EFI
parted $DISK name 2 boot
parted $DISK name 3 cryptdata
parted $DISK set 1 esp on
parted $DISK unit MiB print

cryptsetup luksFormat $(blkid -t PARTLABEL=cryptdata -o device)
```

## open luks disk

Set `LUKS_DISK` to the luks partition as needed

```
cryptsetup luksOpen "$(blkid -t PARTLABEL=cryptdata -o device)" cryptdata
```

## lvm setup (optional)

Skip this step lvm volumes have already been set up

```
pvcreate /dev/mapper/cryptdata
vgcreate ssd /dev/mapper/cryptdata
export SWAP_SIZE_KiB=$(grep MemTotal /proc/meminfo | awk '{print $2*1.5}')
echo "${SWAP_SIZE_KiB}"
lvcreate ssd -n swap -L "${SWAP_SIZE_KiB}KiB"
lvcreate ssd -n system -L 25GiB
lvcreate ssd -n data -l 50%FREE
lvdisplay
vgdisplay
```
## create swap & filesystems (optional)

Skip this step if filesystems have already been created

```
mkswap /dev/ssd/swap
mkfs.vfat -F 32 -n EFI $(blkid -t PARTLABEL=EFI -o device)
```

## install ubuntu

```
Welcome -> continue
Choose keyboard layout -> Continue
Minimal Installation, Download updates while Installing Ubuntu, Install third-party software... -> Continue
Something else -> Continue
  ssd-data -> change -> use as btrfs -> (format if first install) -> mount point /home
  ssd-root -> change -> use as btrfs -> (format if first install) -> mount point /
  /dev/vda1 efi partition -> change -> use as EFI System partition
  /dev/vda2 boot partition -> change -> use as ext4 -> format -> mount point /boot
  device for boot loader -> /dev/vda
  install now -> continue
Where are you -> Berlin (Or your time zone) -> Continue
Who are you -> Add your user details -> Continue
At the end choose "Continue testing"
```

## mount volumes

```
mkdir -p /mnt/system
mount -t btrfs -o noatime,ssd,discard,subvol=/ /dev/ssd/system /mnt/system
mkdir -p /mnt/data
mount -t btrfs -o noatime,ssd,discard,subvol=/ /dev/ssd/data /mnt/data
```

## move dirs to data volume

Skip this step if data partition has not been reformatted

```
for src in opt srv var usr/local; do
  dest="${src//\//_}"
  btrfs subvolume create "/mnt/data/@${dest}"
  echo "Syncing /mnt/system/@/${src} to /mnt/data/@${dest}"
  rsync -azv "/mnt/system/@/${src}/" "/mnt/data/@${dest}"
  touch "/mnt/data/@${dest}/.synced"
done
```

## fix installation

```
btrfs subvolume set-default /mnt/system/@

cat <<EOF > /mnt/system/@/etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
UUID=$(blkid -s UUID -o value -t PARTLABEL=boot) /boot           ext4    defaults,noatime        0       2
UUID=$(blkid -s UUID -o value -t PARTLABEL=EFI)  /boot/efi       vfat    umask=0077      0       1
/dev/mapper/ssd-swap   none            swap    sw              0       0
/dev/mapper/ssd-system /               btrfs   defaults,noatime,ssd,discard 0       1
/dev/mapper/ssd-data   /var            btrfs   defaults,noatime,ssd,discard,subvol=@var 0       1
/dev/mapper/ssd-data   /opt            btrfs   defaults,noatime,ssd,discard,subvol=@opt 0       1
/dev/mapper/ssd-data   /srv            btrfs   defaults,noatime,ssd,discard,subvol=@srv 0       1
/dev/mapper/ssd-data   /usr/local      btrfs   defaults,noatime,ssd,discard,subvol=@usr_local 0       1
/dev/mapper/ssd-data   /home           btrfs   defaults,noatime,ssd,discard,subvol=@home 0       2
EOF

cat <<-EOF > /mnt/system/@/etc/crypttab
cryptdata UUID=$(blkid -s UUID -o value -t PARTLABEL=cryptdata) none    luks,discard
EOF

patch /mnt/system/@/etc/grub.d/10_linux <<'EOF'
--- 10_linux	2022-05-18 21:33:57.149155886 +0200
+++ 10_linux	2022-05-18 21:22:50.566606926 +0200
@@ -126,6 +125,0 @@
-    xbtrfs)
-	rootsubvol="`make_system_path_relative_to_its_root /`"
-	rootsubvol="${rootsubvol#/}"
-	if [ "x${rootsubvol}" != x ]; then
-	    GRUB_CMDLINE_LINUX="rootflags=subvol=${rootsubvol} ${GRUB_CMDLINE_LINUX}"
-	fi;;
EOF
```

## chroot to newroot

```
mkdir -p /target
mount -t btrfs -o noatime,ssd,discard,subvol=/@ /dev/ssd/system /target

for d in dev dev/pts sys proc run etc/resolv.conf; do
  mount -B /$d /target/$d
done

chroot /target /bin/bash
```


## fix initramfs & grub

```
mount -a

update-initramfs -u -k all
update-grub
```

## install & configure snapper

```
apt update
apt install snapper -y

snapper --no-dbus -c root create-config /

cat <<EOF >> /etc/fstab
/dev/mapper/ssd-system /.snapshots     btrfs   defaults,noatime,ssd,discard,subvol=@/.snapshots 0       1
EOF
mount -a

patch /etc/snapper/configs/root <<'EOF'
--- root	2022-05-18 22:26:28.024896904 +0200
+++ root.new	2022-05-18 22:28:16.482823834 +0200
@@ -44 +44 @@
-TIMELINE_CREATE="yes"
+TIMELINE_CREATE="no"
EOF

systemctl disable snapper-boot.timer

snapper --no-dbus -c root create -t single -d "first root filesystem" --read-write
btrfs subvolume set-default /.snapshots/1/snapshot
```

## exit chroot & reboot

```
umount -a
exit

for d in dev/pts dev sys proc; do
  umount /target/$d
done
umount /target

reboot
```

# troubleshooting

## reset

```
swapoff /dev/ssd/swap
cryptsetup luksClose /dev/ssd/swap
cryptsetup luksClose /dev/ssd/home
cryptsetup luksClose /dev/ssd/root
cryptsetup luksClose /dev/mapper/cryptdata
```

## reset btrfs

```
btrfs subvolume delete /mnt/@/var
btrfs subvolume delete /mnt/@/usr/local
btrfs subvolume delete /mnt/@/srv
btrfs subvolume delete /mnt/@/opt
btrfs subvolume delete /mnt/@/.snapshots/1/snapshot
btrfs subvolume delete /mnt/@/.snapshots
btrfs subvolume delete /mnt/@
btrfs subvolume sync /mnt
```
