#!/usr/bin/env bash

set -euo pipefail

log() {
    echo "$@" >&2
}

die() {
    local code
    log "$1, aborting"
    exit ${2:-1}
}

if [[ ! -e .venv ]]; then
    die "Not bootstrapped, run bootstrap.sh then try again"
fi

source .venv/bin/activate

PLAYBOOK=${PLAYBOOK:-"playbooks/setup-workstation.yml"}
HOST=${HOST:-"$(hostname -s)"}

ansible-playbook "${PLAYBOOK}" -l "${HOST}" $@
