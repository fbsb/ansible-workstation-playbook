#!/usr/bin/env bash

set -euo pipefail

log() {
    echo "$@" >&2
}

die() {
    local code
    log "$1, aborting"
    exit ${2:-1}
}

clt_installed() {
    [[ -e "/Library/Developer/CommandLineTools/usr/bin/git" ]]
}

install_clt() {
    local since=0
    local sleep=10
    log "Installing command line tools requires manual intervention"
    while /usr/bin/xcode-select --install > /dev/null 2>&1; do
        log "Waiting 10s for installation to finish since ${since}s"
        sleep $sleep
        since=$((since+$sleep))
    done
}

if [[ "$(uname)" == "Darwin" ]]; then
    log "Checking Command Line Tools"
    if clt_installed; then
        log "Command Line Tools are installed"
    else
        install_clt || die "Could not install Command Line Tools"
        log "Command Line Tools installed successfully"
    fi
fi

if command -v apt-get >/dev/null; then
    sudo apt-get update
    sudo apt-get install -y git python3-venv
fi

log "Create venv"
python3 -m venv .venv --prompt "ansible_venv"

log "Activate venv"
source .venv/bin/activate

log "Upgrade pip"
python3 -m pip install --upgrade pip

log "Install wheel"
python3 -m pip install wheel

log "Install python requirements"
python3 -m pip install -r requirements.txt

log "Install ansible requirements"
ansible-galaxy install -r requirements.yml

log "# activate venv by running"
log "source .venv/bin/activate"
